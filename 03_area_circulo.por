// Algoritmo: Área do círculo
// Descrição: recebe do usuário o valor do raio de um círculo e,
//		    em seguida, calcula a sua área.
// Autor: Alyppyo Coutinho
// Data: 14/03/2019
programa
{
	// Bibliotecas
	inclua biblioteca Matematica --> mat

	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		real raio // Valor do raio do círculo (fornecido pelo usuário)
		real area // Área calculada do círculo

		// Apresentação do programa
		escreva("-- Área do Círculo --\n")
	
		// Solicitar o valor do raio do círculo
		escreva("- Entre com o raio do círculo: ")
		leia(raio)
		
		// Calcular a área do círculo
		area = mat.PI * mat.potencia(raio, 2.0)
		
		// Divulgar a área do círculo
		escreva("> Área do círculo: ", area)
	}
}

/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 631; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */