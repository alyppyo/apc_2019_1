// Algoritmo: Selection sort
// Descrição: versão do algoritmo de ordenação Selection Sort.
// Autor: Alyppyo Coutinho
// Data: 11/06/2019
programa
{
	// Bibliotecas
	inclua biblioteca Util --> ut

	// Função principal
	funcao inicio()
	{
		// Declaração de constantes
		const inteiro TAM = 10
		
		// Declaração de variáveis
		inteiro vet[TAM]
		inteiro aux, iMenor

		// Preencher o vetor
		preencheVetor(vet)

		// Apresentação do programa
		escreva("-- Selection Sort --\n")

		// Imprimir o vetor
		escreva("> Vetor original: ")
		escrevaVetor(vet)

		// Ordenar o vetor via Selection sort
		para(inteiro j = 0; j < TAM-1; j++) {
			iMenor = j
			para(inteiro i = j+1; i < TAM; i++) {
				se(vet[i] < vet[iMenor])
					iMenor = i
			}
	
			se(iMenor != j) {
				aux = vet[j]
				vet[j] = vet[iMenor]
				vet[iMenor] = aux
			}
		}

		// Imprimir o vetor ordenado
		escreva("\n> Vetor ordenado: ")
		escrevaVetor(vet)
	}

	// Função: escrevaVetor
	// Descrição: imprime o conteúdo de um vetor passado
	funcao escrevaVetor(inteiro vet[]) {
		para(inteiro i = 0; i < ut.numero_elementos(vet); i++)
			escreva(vet[i], " ")
	}

	// Função: preencheVetor
	// Descrição: preenche um vetor com valores aleatórios
	funcao preencheVetor(inteiro & vet[]) {
		para(inteiro i = 0; i < ut.numero_elementos(vet); i++)
			vet[i] = ut.sorteia(1, 100)
	}
}



/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 505; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */