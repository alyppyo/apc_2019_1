// Algoritmo: Troca
// Descrição: troca o valor de duas variáveis através de uma função específica.
// Autor: Alyppyo Coutinho
// Data: 23/04/2019
programa
{
	// Função principal	
	funcao inicio()
	{
		// Declaração de variáveis
		inteiro x = 10, y = 20

		// Trocar o conteúdo das variáveis
		troca(x, y)

		// Divulgar o valor das variáveis
		escreva("x = ", x, ", y = ", y)
	}

	// Função: troca
	// Descrição: trocar o contéudo de duas variáveis passadas como parâmetro.
	funcao troca(inteiro & a, inteiro & b)
	{
		// Declaração de variáveis
		inteiro aux = a
		a = b
		b = aux
	}
}

/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 582; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */