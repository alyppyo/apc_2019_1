// Algoritmo: Proteção de tela
// Descrição: apresenta um texto de proteção de tela, o qual se movimenta pela
//		    janela alternando sua direção no momento em que colide com as sua
//		    borda.
// Autor: Alyppyo Coutinho
// Data: 16/05/2019
programa
{
	// Bibiliotecas
	inclua biblioteca Graficos --> grf
	inclua biblioteca Teclado --> tcl
	inclua biblioteca Util --> ut

	// Função principal
	funcao inicio()
	{
		// Declaração de constantes
		const inteiro LARGURA_JANELA = 600, ALTURA_JANELA = 400

		// Declaração de variáveis
		inteiro x = 0

		// Iniciar o modo gráfico
		grf.iniciar_modo_grafico(falso)		

		// Configurar janela
		grf.definir_dimensoes_janela(LARGURA_JANELA, ALTURA_JANELA)
		grf.definir_titulo_janela("Proteção de Tela")

		// Configurar o texto
		grf.definir_fonte_texto("Trebuchet MS")
		grf.definir_estilo_texto(verdadeiro, verdadeiro, falso)
		grf.definir_tamanho_texto(32.0)

		// Loop principal do modo gráfico
		enquanto(verdadeiro) {
			// Desenhar texto
			grf.definir_cor(grf.COR_BRANCO)
			grf.desenhar_texto(x, 100, "Alyppyo Coutinho")
			x++
			
			// Renderizar a tela
			grf.renderizar()

			// Aguardar um tempo antes de atualizar
			ut.aguarde(20)
			
			// Interromper o laço caso o usuário pressione a tecla ESC
			se(tcl.tecla_pressionada(tcl.TECLA_ESC)) pare
		}

		// Encerrar o modo gráfico
		grf.encerrar_modo_grafico()
	}
}




/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1192; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */