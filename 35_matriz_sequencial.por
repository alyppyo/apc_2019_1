// Algoritmo: Matriz sequencial
// Descrição: preenche uma matriz com valores sequenciais e a imprime
//		    em formato de grade.
// Autor: Alyppyo Coutinho
// Data: 21/05/2019
programa
{
	// Função principal
	funcao inicio()
	{
		// Declaração de constantes
		const inteiro LINHAS = 5, COLUNAS = 5

		// Declaração de variáveis
		inteiro matriz[LINHAS][COLUNAS], cont = 1

		// Apresentação do programa
		escreva("-- Matriz Sequencial --\n")

		// Preencher e divulgar a matriz
		para(inteiro i = 0; i < LINHAS; i++) {
			// Imprimir linhas
			para(inteiro j = 0; j < COLUNAS; j++) {
				// Preencher a matriz
				matriz[i][j] = cont
				cont++

				// Imprimir o conteúdo da matriz
				se(matriz[i][j] < 10) escreva(0)
				escreva(matriz[i][j], "\t")
			}

			// Inserir uma quebra de linha para formatação
			escreva("\n")
		}
	}
}







/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 725; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */