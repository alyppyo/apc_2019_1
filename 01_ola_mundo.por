// Algoritmo: Olá, APC!
// Descrição: programa básico para teste do Portugol Studio e definição dos comentários básicos.
// Autor: Alyppyo Coutinho
// Data: 28/03/2019
programa
{
	// Função principal
	funcao inicio()
	{
		escreva("Olá, APC!")
	}
}

/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 248; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */