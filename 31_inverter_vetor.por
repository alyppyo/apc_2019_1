// Algoritmo: Inverter vetor
// Descrição: recebe um vetor do usuário e o inverte.
// Autor: Alyppyo Coutinho
// Data: 14/05/2019
programa
{
	
// Função principal
	funcao inicio()
	{
		// Declaração de constantes
		const inteiro TAM = 20
	
		// Declaração de variáveis
		inteiro vet[TAM]
		inteiro qntValores, aux

		// Apresentação do programa
		escreva("-- Inverter Vetor --\n")

		// Solicitar a quantidade de elementos
		faca {
			escreva("- Entre com a quantidade de elementos (máx. ", TAM, "): ")
			leia(qntValores)

			// Avisar ao usuário em caso de valor inválido
			se(qntValores < 1 ou qntValores > TAM)
				escreva("> Valor inválido! A quantidade de elementos deve estar entre 1 e ", TAM,"!\n")
		} enquanto(qntValores < 1 ou qntValores > TAM)

		// Solicitar os valores ao usuário
		para(inteiro i = 0; i < qntValores; i++) {
			escreva("- Entre com o ", i+1, "º valor: ")
			leia(vet[i])
		}

		// Limpar a tela e reapresentar o programa
		limpa()
		escreva("-- Inverter Vetor --\n")

		// Apresentar o conteúdo do vetor original
		escreva("> Vetor original: ")		
		para(inteiro i = 0; i < qntValores; i++) {
			escreva(vet[i], " ")
		}

		// Inverter o vetor
		para(inteiro i = 0; i < qntValores/2; i++) {
			aux = vet[i]
			vet[i] = vet[qntValores-1-i]
			vet[qntValores-1-i] = aux
		}
		
		// Apresentar o conteúdo do vetor invertido
		escreva("\n> Vetor invertido: ")		
		para(inteiro i = 0; i < qntValores; i++) {
			escreva(vet[i], " ")
		}
	}
}




/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1213; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */