// Algoritmo: Média ponderada
// Descrição: recebe 3 notas e 3 pesos e calcula a média ponderada resultante.
// Autor: Alyppyo Coutinho
// Data: 16/04/2019
programa
{
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		real nota1, nota2, nota3
		inteiro peso1, peso2, peso3

		// Apresentação do programa
		escreva("-- Média Ponderada --\n")

		// Solicitar notas e pesos
		escreva("- Entre com a primeira nota: ")		
		leia(nota1)

		escreva("- Entre com o peso da primeira nota: ")
		leia(peso1)

		escreva("\n- Entre com a segunda nota: ")		
		leia(nota2)

		escreva("- Entre com o peso da segunda nota: ")
		leia(peso2)

		escreva("\n- Entre com a terceira nota: ")		
		leia(nota3)

		escreva("- Entre com o peso da terceira nota: ")
		leia(peso3)

		// Divulgar a média ponderada
		escreva("\n> Média ponderada: ", mediaPonderada(nota1, peso1, nota2, peso2, nota3, peso3))
	}


	// Função: mediaPonderada
	// Descrição: recebe 3 notas e 3 pesos e calcula a média ponderada resultante.
	funcao real mediaPonderada(real nota1, inteiro peso1,
						  real nota2, inteiro peso2,
						  real nota3, inteiro peso3)
	{
		// Retornar o valor da média
		retorne (nota1*peso1 + nota2*peso2 + nota3*peso3) / (peso1+peso2+peso3)
	}
}







/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 899; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */