// Algoritmo: Carga pesada
// Descrição: recebe diversas cargas e as separa por polaridade em duas bandejas.
// Autor: Alyppyo Coutinho
// Data: 06/06/2019
programa
{
	// Função principal
	funcao inicio()
	{
		// Declaração de constantes
		const inteiro TAM = 20
		inteiro bandejaPositiva[TAM], bandejaNegativa[TAM]
		inteiro carga, qntCargasPositivas = 0, qntCargasNegativas = 0
		inteiro cargaTotalPositivas = 0, cargaTotalNegativas = 0

		// Apresentação do programa
		escreva("-- Carga Pesada --\n")		

		// Solicitar as cargas ao usuário
		faca {
			// Solicitar as cargas ao usuário
			escreva("- Entre com um valor de carga: ")
			leia(carga)

			// Separar as cargas nas bandejas
			se(carga > 0) { // Enviar carga para bandeja positiva
				se(qntCargasPositivas < TAM) {
					bandejaPositiva[qntCargasPositivas] = carga
					cargaTotalPositivas += carga
					qntCargasPositivas++					
				}
				senao {
					escreva("> Bandeja positiva cheia! Não é possível inserir mais cargas!\n")
				}
			}
			senao se(carga < 0) {
				se(qntCargasNegativas < TAM) {
					bandejaNegativa[qntCargasNegativas] = carga
					cargaTotalNegativas += carga
					qntCargasNegativas++					
				}
				senao {
					escreva("> Bandeja negativa cheia! Não é possível inserir mais cargas!\n")
				}
			}
		} enquanto(carga != 0)

		// Divulgar catalogação das bandejas
		
		// Para a bandeja positiva
		escreva("\n-- Bandeja Positiva --")
		escreva("\n> Quantidade de cargas: ", qntCargasPositivas)
		escreva("\n> Soma total: ", cargaTotalPositivas)
		escreva("\n> Média das cargas: ")
		// Verificar se a bandeja não está vazia
		se(qntCargasPositivas > 0) escreva(cargaTotalPositivas/(qntCargasPositivas+0.0))
		senao escreva(0)

		// Para a bandeja negativa
		escreva("\n\n-- Bandeja Negativa --")
		escreva("\n> Quantidade de cargas: ", qntCargasNegativas)
		escreva("\n> Soma total: ", cargaTotalNegativas)
		escreva("\n> Média das cargas: ")
		// Verificar se a bandeja não está vazia
		se(qntCargasNegativas > 0) escreva(cargaTotalNegativas/(qntCargasNegativas+0.0))
		senao escreva(0)
	}
}


/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1761; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */