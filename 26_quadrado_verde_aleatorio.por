// Algoritmo: Quadrado verde em posição aleatória
// Descrição: desenhar um quadrado verde em posições aleatórias a cada vez que o
//		    algoritmo é executado.
// Autor: Alyppyo Coutinho
// Data: 30/04/2019
programa
{
	// Biblioteca
	inclua biblioteca Graficos --> grf
	inclua biblioteca Mouse --> mse
	inclua biblioteca Util --> ut
	
	// Função principal
	funcao inicio()
	{
		// Declaração de constantes
		const inteiro LARGURA_JANELA = 500, ALTURA_JANELA = 600
		const inteiro LADO_QUADRADO = 100
	
		// Iniciar o modo gráfico
		grf.iniciar_modo_grafico(falso)

		// Definir dimensões da janela
		grf.definir_dimensoes_janela(LARGURA_JANELA, ALTURA_JANELA)

		// Definir o título da janela (apresentação do programa)
		grf.definir_titulo_janela("Quadrado Verde em Posição Aleatória")

		// Desenhar retângulo
		grf.definir_cor(grf.COR_VERDE)
		grf.desenhar_retangulo(ut.sorteia(0, LARGURA_JANELA-LADO_QUADRADO), // Coordenada x (canto superior esquerdo)
						   ut.sorteia(0, ALTURA_JANELA-LADO_QUADRADO),  // Coordenada y (canto superior esquerdo)
						   LADO_QUADRADO,    // Largura do retângulo
						   LADO_QUADRADO,	 // Altura do retângulo
						   falso,			 // Cantos não arredondados
						   verdadeiro)       // Preenchimento do retângulo

		// Renderizar a tela
		grf.renderizar()
		
		// Segurar a janela
		mse.ler_botao()

		// Encerrar o modo gráfico
		grf.encerrar_modo_grafico()
	}
}

/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 550; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */