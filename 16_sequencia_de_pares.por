// Algoritmo: Sequência de pares
// Descrição: recebe um valor natural par do usuário e apresenta
//		    todos os valores pares a partir de 2 até o valor fornecido.
// Autor: Alyppyo Coutinho
// Data: 28/03/2019
programa
{
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		inteiro cont = 2  // Contador que irá armazenar os pares
		inteiro limite    // Valor limite fornecido pelo usuário
		
		// Apresentação do programa
		escreva("-- Sequência de Pares --\n")
		
		// Solicitação do valor par final
		escreva("- Entre com o número natural par que finalizará a sequência: ")
		leia(limite)

		// Validar se o valor informado pelo usuário é natural e par
		se(limite > -1 e limite%2 == 0) {
			// Imprimir os valores pares de 2 até o valor fornecido
			escreva("\n> Pares: ")
			enquanto(cont <= limite) {
				// Impressão dos valores pares
				escreva(cont, " ")
	
				// Atualização da variável de controle
				cont += 2
			}
		}
		senao {
			escreva("\n> O número fornecido (", limite, ") não é natural e par!")
		}
	}
}



/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1035; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */