// Algoritmo: Matriz sequencial
// Descri��o: preenche a matriz com valores sequenciais.
// Autor: Alyppyo Coutinho
// Data: 21/05/2019
programa
{
	// Fun��o principal
	funcao inicio()
	{
		// Declara��o de constantes
		const inteiro LINHAS = 5, COLUNAS = 5
		
		// Declara��o de vari�veis
		inteiro matriz[LINHAS][COLUNAS], cont = 1

		// Apresenta��o do programa
		escreva("-- Matriz Sequencial --\n")		

		// La�o para preenchimento e divulga��o da matriz
		para(inteiro i = 0; i < LINHAS; i++) {
			para(inteiro j = 0; j < COLUNAS; j++) {
				// Preenchimento das c�lulas
				matriz[i][j] = cont
				cont++

				// Divulga��o dos valores
				se(matriz[i][j] < 10) escreva(0)
				escreva(matriz[i][j], "\t")
			}
			
			// Quebra de linha para formata��o da grade
			escreva("\n")
		}

		// Apresenta��o do programa
		escreva("-- Diagonal Principal --\n")		

		// La�o para preenchimento e divulga��o da matriz
		para(inteiro i = 0; i < LINHAS; i++) {
			para(inteiro j = 0; j < COLUNAS; j++) {
				// Divulga��o dos valores
				se(i == j) {
					se(matriz[i][j] < 10) escreva(0)
					escreva(matriz[i][j], "\t")
				}
				senao escreva("  \t")
			}
			
			// Quebra de linha para formata��o da grade
			escreva("\n")
		}

		// Apresenta��o do programa
		escreva("-- Diagonal Secund�ria --\n")		

		// La�o para preenchimento e divulga��o da matriz
		para(inteiro i = 0; i < LINHAS; i++) {
			para(inteiro j = 0; j < COLUNAS; j++) {
				// Divulga��o dos valores
				se(i+j == LINHAS-1) {
					se(matriz[i][j] < 10) escreva(0)
					escreva(matriz[i][j], "\t")
				}
				senao escreva("  \t")
			}
			
			// Quebra de linha para formata��o da grade
			escreva("\n")
		}

		// Apresenta��o do programa
		escreva("-- Ambas as diagonais --\n")		

		// La�o para preenchimento e divulga��o da matriz
		para(inteiro i = 0; i < LINHAS; i++) {
			para(inteiro j = 0; j < COLUNAS; j++) {
				// Divulga��o dos valores
				se(i == j ou i+j == LINHAS-1) {
					se(matriz[i][j] < 10) escreva(0)
					escreva(matriz[i][j], "\t")
				}
				senao escreva("  \t")
			}
			
			// Quebra de linha para formata��o da grade
			escreva("\n")
		}
	}
}







/* $$$ Portugol Studio $$$ 
 * 
 * Esta se��o do arquivo guarda informa��es do Portugol Studio.
 * Voc� pode apag�-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1929; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */