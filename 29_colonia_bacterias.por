// Algoritmo: Colônias de bactérias
// Descrição: recebe a quantidade inicial de bactérias em duas
//		    colônias e indica quanto tempo levará para uma
//		    ultrapassar a outra.
// Autor: Alyppyo Coutinho
// Data: 14/05/2019
programa
{	
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		real colX, colY // Quantidade de elementos na colônia
		inteiro horas = 0

		// Apresentação do programa
		escreva("-- Colônias de Bactérias --\n")

		// Solicitação da população inicial de cada colônia
		escreva("- Entre com a quantidade de bactérias na colônia X: ")
		leia(colX)

		escreva("- Entre com a quantidade de bactérias na colônia Y: ")
		leia(colY)

		// Verificar quantas horas se passaram até que a colônia X ultrapasse a Y
		enquanto(colX <= colY) {
			colX *= 1.05
			colY *= 1.02
			horas++
		}

		// Divulgar a quantidade de horas
		escreva("> Quantidade de horas necessárias: ", horas)
	}
}




/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 934; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */