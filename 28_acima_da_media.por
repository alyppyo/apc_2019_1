// Algoritmo: Acima da média
// Descrição: recebe até 20 valores do usuário, calcula sua média e indica quais
//		    dos valores fornecidos estão acima dela.
// Autor: Alyppyo Coutinho
// Data: 07/05/2019
programa
{
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		real vetor[20], media = 0.0
		inteiro qntElementos

		// Apresentação do programa
		escreva("-- Acima da Média --\n")

		// Solicitando a quantidade máxima de elementos no vetor
		faca {
			escreva("- Entre com a quantidade de elementos do vetor (máx. 20): ")
			leia(qntElementos)

			// Verificar se o usuário entrou com um valor errado
			se(qntElementos < 1 ou qntElementos > 20)
				escreva("> Valor inválido! A quantidade de elementos deve ser entre 1 e 20!\n")
		} enquanto(qntElementos < 1 ou qntElementos > 20)

		// Limpar a tela e iniciar coleta de dados
		limpa()
		
		// Reapresentação do programa
		escreva("-- Acima da Média --\n")

		// Solicitar os elementos ao usuário
		para(inteiro i = 0; i < qntElementos; i++) {
			escreva("- Entre com o ", i+1 , "º elemento: ")
			leia(vetor[i])		

			// Acumular valores para o cálculo da média
			media += vetor[i]
		}

		// Calcular a média
		media /= qntElementos

		// Limpar a tela e divulgar os resultados
		limpa()
		
		// Reapresentação do programa
		escreva("-- Acima da Média --\n")

		// Percorrer o vetor buscando os valores que estão acima da média
		escreva("> Média: ", media)
		escreva("\n> Valores acima da média: ")
		para(inteiro i = 0; i < qntElementos; i++) {
			// Verificar se o valor está acima da média
			se(vetor[i] > media)
				escreva(vetor[i], " ")
		}
	}
}



/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1457; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */