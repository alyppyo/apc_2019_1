// Algoritmo: Número de 3 dígitos
// Descrição: recebe um número de 3 dígitos, extraindo cada um deles
//		    de acordo com sua posição.
// Autor: Alyppyo Coutinho
// Data: 19/03/2019
programa
{
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		inteiro numero
		
		// Apresentação do programa
		escreva("-- Número de 3 Dígitos --\n")
		
		// Solicitação do número de 3 dígitos
		escreva("- Entre com um número de 3 dígitos (100-999): ")
		leia(numero)
		
		// Separação e divulgação do número de 3 dígitos
		escreva("\n> Centenas: ", numero/100)
		escreva("\n> Dezenas: ", (numero%100)/10)
		escreva("\n> Unidades: ", numero%10)
	}
}



/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 236; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */