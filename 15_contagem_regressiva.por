// Algoritmo: Contagem regressiva
// Descrição: realiza uma contagem regressiva até zero a partir
//		    do número fornecido pelo usuário. A contagem é feita
//		    com intervalo de 1 segundo e limpando a tela a cada
//		    atualização.
// Autor: Alyppyo Coutinho
// Data: 28/03/2019
programa
{
	// Bibliotecas
	inclua biblioteca Util --> ut
	
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		inteiro cont
		
		// Apresentação do programa
		escreva("-- Contagem Regressiva --\n")

		// Solicitar ao usuário quantidade de segundos
		escreva("- Entre com a quantidade de segundos da contagem: ")
		leia(cont)
		
		// Impressão dos valores
		enquanto(cont >= 0) {
			// Limpar a tela
			limpa()

			// Apresentação do programa
			escreva("-- Contagem Regressiva --\n")

			// Imprimir valores da contagem
			escreva(cont, " ")

			// Aguardar 1 segundo entre as impressões
			ut.aguarde(1000)

			// Possibilidades de atualização
			// 1) cont = cont - 1
			// 2) cont -= 1
			cont--
		}
	}
}

/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 850; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */