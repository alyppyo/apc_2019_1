// Algoritmo: Colisão entre círculos
// Descrição: dadas as coordenadas cartesianas e o tamanho do raio de
//		    de dois círculos, indica se eles estão em colisão.
// Autor: Alyppyo Coutinho
// Data: 21/03/2019
programa
{
	// Bibliotecas
	inclua biblioteca Matematica --> mat

	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		real x1, y1, raio1 // Dados do primeiro círculo
		real x2, y2, raio2 // Dados do segundo círculo
		real distancia // Distância entre os centros dos círculos
		
		// Apresentação do programa
		escreva("-- Colisão entre Círculos --\n")
		
		// Solicitar os dados do primeiro círculo
		escreva("- Entre com a coordenada x do centro do primeiro círculo: ")
		leia(x1)

		escreva("- Entre com a coordenada y do centro do primeiro círculo: ")
		leia(y1)

		escreva("- Entre com o raio do primeiro círculo: ")
		leia(raio1)
		
		// Solicitar os dados do segundo círculo
		escreva("- Entre com a coordenada x do centro do segundo círculo: ")
		leia(x2)

		escreva("- Entre com a coordenada y do centro do segundo círculo: ")
		leia(y2)

		escreva("- Entre com o raio do segundo círculo: ")
		leia(raio2)
		
		// Calcular a distância
		distancia = mat.raiz(mat.potencia(x2-x1,2.0) + mat.potencia(y2-y1,2.0), 2.0)
		
		// Verificar se os círculos estão em colisão
		se (distancia < raio1+raio2) {
			escreva("> Os círculos estão colidindo!")
		}
		senao {
			escreva("> Os círculos não estão colidindo!")
		}
	}
}



/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1262; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */