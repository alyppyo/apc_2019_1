// Algoritmo: X primeiros múltiplos de Y
// Descrição: dado um número Y e um valor X, imprime os primeiros
//		    X múltiplos de Y (com exceção dele próprio).
// Autor: Alyppyo Coutinho
// Data: 28/03/2019
programa
{	
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		inteiro numero // Número que terá seus múltiplos listados
		inteiro qntMultiplos // Quantidade de múltiplos a serem listados
		inteiro cont = 2 // Contador de múltiplos

		// Apresentação do programa
		escreva("-- X Primeiros Múltiplos de Y --\n")
		
		// Solicitar o número ao usuário
		escreva("- Entre com um número: ")
		leia(numero)
		
		// Solicitar a quantidade desejada de múltiplos do número		
		escreva("- Entre com a quantidade de múltiplos: ")
		leia(qntMultiplos)
		
		// Imprimir os múltiplos solicitados
		escreva(qntMultiplos, " primeiros múltiplos: ")
		enquanto(cont <= qntMultiplos+1) {
			escreva(numero*cont, " ")
			cont++
		}
	}
}




/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 954; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */