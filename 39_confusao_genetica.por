// Algoritmo: Confusão genética
// Descrição: compara duas cadeias de polinucleotídeos e indica se são compatíveis.
// Autor: Alyppyo Coutinho
// Data: 06/06/2019
programa
{
	// Biblioteca
	inclua biblioteca Texto --> txt

	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		cadeia cadeia1, cadeia2
		caracter carac1, carac2
		inteiro tam1, tam2
		logico cadeiasCompativeis = verdadeiro

		// Apresentação do programa
		escreva("-- Confusão Genética --\n")

		// Solicitação das cadeias ao usuário
		escreva("- Entre com a primeira cadeia: ")
		leia(cadeia1)

		escreva("- Entre com a segunda cadeia: ")
		leia(cadeia2)

		// Obter o tamanho das cadeias
		tam1 = txt.numero_caracteres(cadeia1)
		tam2 = txt.numero_caracteres(cadeia2)

		// Verificar o tamanho das cadeias
		se(tam1 != tam2) {
			// Avisar ao usuário sobre o problema
			escreva("> As cadeias fornecidas têm tamanhos diferentes, logo não são compatíveis!")
		}
		senao {
			// Converter as cadeias para maiúsculo
			cadeia1 = txt.caixa_alta(cadeia1)
			cadeia2 = txt.caixa_alta(cadeia2)

			// Percorrer as cadeias verificando se existe algum caractere indevido
			para(inteiro i = 0; i < tam1; i++) {
				carac1 = txt.obter_caracter(cadeia1, i)
				carac2 = txt.obter_caracter(cadeia2, i)
				se((carac1 != 'A' e carac1 != 'T' e carac1 != 'C' e carac1 != 'G') ou 		
				   (carac2 != 'A' e carac2 != 'T' e carac2 != 'C' e carac2 != 'G')) {
					escreva("> As cadeias possuem caracteres inválidos, logo não são válidas!")
					cadeiasCompativeis = falso
					pare  	
				}
				senao {
					se(nao ((carac1 == 'A' e carac2 == 'T') ou
					   	   (carac1 == 'T' e carac2 == 'A') ou
					   	   (carac1 == 'C' e carac2 == 'G') ou
					   	   (carac1 == 'G' e carac2 == 'C'))) {
						escreva("> As cadeias não são compatíveis!")
						cadeiasCompativeis = falso
					   	pare
					}
				}
			}

			// Indicar se as cadeias são compatíveis
			se(cadeiasCompativeis) 
				escreva("> As cadeias são compatíveis!")
		}
	}
}

/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1996; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */