// Algoritmo: Área do quadrado
// Descrição: recebe do usuário o valor do lado de um quadrado e,
//		    em seguida, calcula a sua área.
// Autor: Alyppyo Coutinho
// Data: 14/03/2019
programa
{
	// Bibliotecas
	inclua biblioteca Matematica

	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		real lado // Valor do lado do quadrado (fornecido pelo usuário)
		real area // Área calculada do quadrado

		// Apresentação do programa
		escreva("-- Área do Quadrado --\n")
	
		// Solicitar o valor do lado do quadrado
		escreva("- Entre com o valor do lado: ")
		leia(lado)
		
		// Calcular a área do quadrado
		// area = lado*lado
		area = Matematica.potencia(lado, 2.0)
		
		// Divulgar a área do quadrado	
		escreva("> Área do quadrado: ", area)
	}
}





/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 691; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */