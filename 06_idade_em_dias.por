// Algoritmo: Idade em dias
// Descrição: recebe a idade do usuário em anos, meses e dias e calcula
//		    a quantidade total de dias vividos.
// Autor: Alyppyo Coutinho
// Data: 19/03/2019
programa
{
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		inteiro anos, meses, dias // Idade fornecida pelo usuário
		inteiro totalDias // Quantidade total de dias da idade do usuário
		
		// Apresentação do programa
		escreva("-- Idade em Dias --\n")
		
		// Solicitar ao usuário sua idade em anos, meses e dias
		escreva("- Entre com a quantidade de anos: ")
		leia(anos)

		escreva("- Entre com a quantidade de meses: ")
		leia(meses)

		escreva("- Entre com a quantidade de dias: ")
		leia(dias)
		
		// Calcular a quantidade de dias
		totalDias = anos*365 + meses*30 + dias
		
		// Divulgar o resultado
		escreva("\n> Total de dias vividos: ", totalDias)
	}
}


/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 889; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */