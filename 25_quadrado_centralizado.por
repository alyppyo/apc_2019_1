// Algoritmo: Quadrado branco centralizado
// Descrição: exemplo de uso do modo gráfico no Portugol Studio, desenhando um
//		    quadrado branco centralizado na tela.
// Autor: Alyppyo Coutinho
// Data: 30/04/2019
programa
{
	// Biblioteca
	inclua biblioteca Graficos --> grf
	inclua biblioteca Mouse --> mse

	// Função principal
	funcao inicio()
	{
		// Declaração de constantes
		const inteiro LARGURA_JANELA = 600, ALTURA_JANELA = 600
		const inteiro LADO_QUADRADO = 100
	
		// Iniciar o modo gráfico
		grf.iniciar_modo_grafico(falso)

		// Definir dimensões da janela
		grf.definir_dimensoes_janela(LARGURA_JANELA, ALTURA_JANELA)

		// Definir o título da janela (apresentação do programa)
		grf.definir_titulo_janela("Quadrado Branco Centralizado")

		// Desenhar retângulo
		grf.definir_cor(grf.COR_BRANCO)
		grf.desenhar_retangulo(LARGURA_JANELA/2-LADO_QUADRADO/2, // Coordenada x (canto superior esquerdo)
						   ALTURA_JANELA/2-LADO_QUADRADO/2,  // Coordenada y (canto superior esquerdo)
						   LADO_QUADRADO,    // Largura do retângulo
						   LADO_QUADRADO,	 // Altura do retângulo
						   falso,			 // Cantos não arredondados
						   verdadeiro)       // Preenchimento do retângulo

		// Renderizar a tela
		grf.renderizar()
		
		// Segurar a janela
		mse.ler_botao()

		// Encerrar o modo gráfico
		grf.encerrar_modo_grafico()
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1357; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */