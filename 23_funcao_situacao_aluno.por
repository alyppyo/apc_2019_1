// Algoritmo: Situação do aluno da UFRN
// Descrição: recebe 3 notas referentes às 3 unidades de uma disciplina da UFRN,
//		    calcula sua média e indica qual a situação final do aluno.
// Autor: Alyppyo Coutinho
// Data: 16/04/2019
programa
{
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		real nota1, nota2, nota3

		// Apresentação do programa
		escreva("-- Situação do Aluno da UFRN --\n")

		// Solicitar notas do aluno
		escreva("- Entre com a nota da Unidade I: ")		
		leia(nota1)

		escreva("- Entre com a nota da Unidade II: ")		
		leia(nota2)

		escreva("- Entre com a nota da Unidade III: ")		
		leia(nota3)

		// Divulgar o resultado
		escreva("\n> Situação do aluno: ", situacaoDoAluno(nota1, nota2, nota3) )
	}


	// Função: situacaoDoAluno
	// Descrição: recebe as 3 notas de um aluno e indica se ele foi aprovado,
	//		    aprovado por nota, pra reposição ou reprovado.
	funcao cadeia situacaoDoAluno(real nota1, real nota2, real nota3)
	{
		// Declaração de variáveis
		real notaFinal = (nota1+nota2+nota3)/3

		// Verificar situação do aluno
		se(notaFinal >= 7) 
			retorne "Aprovado"
		senao se(notaFinal >= 5 e nota1 >=3 e nota2 >= 3 e nota3 >= 3)
			retorne "Aprovado por nota"
		senao se(notaFinal >= 3)
			retorne "Na reposição"
		senao
			retorne "Reprovado"
	}
}






/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 736; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */