// Algoritmo: Total de letras em um nome
// Descrição: recebe um nome do usuário e indica a quantidade total de letras
//		    (sem espaços em branco).
// Autor: Alyppyo Coutinho
// Data: 16/05/2019
programa
{
	// Bibliotecas
	inclua biblioteca Texto --> txt

	// Função principal	
	funcao inicio()
	{
		// Declaração de variáveis
		cadeia nome
		inteiro qntLetras = 0

		// Apresentação do programa
		escreva("-- Total de Letras em um Nome --\n")

		// Solicitar um nome ao usuário
		escreva("- Entre com um nome completo: ")
		leia(nome)

		// Divulgar total de caracteres da cadeia
		escreva("\n> Total de caracteres do texto: ", txt.numero_caracteres(nome))

		// Percorrer texto contando apenas os caracteres que não são espaços em branco
		para(inteiro i = 0; i < txt.numero_caracteres(nome); i++) {
			se(txt.obter_caracter(nome, i) != ' ')
				qntLetras++
		}

		// Divulgar a quantidade de letras do nome
		escreva("\n> Quantidade de letras no nome: ", qntLetras)
	}
}







/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 970; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */