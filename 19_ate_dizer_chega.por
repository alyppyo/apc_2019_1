// Algoritmo: Até dizer chega!
// Descrição: recebe do usuário uma lista de nomes encerrando no momento em
//		    que o texto "chega!" é apresentado.
// Autor: Alyppyo Coutinho
// Data: 02/04/2019
programa
{
	// Bibliotecas
	inclua biblioteca Texto --> txt
	
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		cadeia nome
		cadeia listaNomes = ""

		// Apresentação do programa
		escreva("-- Até Dizer Chega! --\n")

		// Recebimento dos nomes
		faca {
			// Solicitação e captura dos nomes
			escreva("- Entre com um nome: ")
			leia(nome)

			// Armazenamento do nome na listagem
			se(txt.caixa_baixa(nome) != "chega!") {
				listaNomes += "\n" + nome
			}
		} enquanto(txt.caixa_baixa(nome) != "chega!")

		// Divulgar lista de nomes
		escreva("\n> Lista:", listaNomes)
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 631; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */