// Algoritmo: Bubble sort
// Descrição: versão do algoritmo de ordenação Bubble Sort.
// Autor: Alyppyo Coutinho
// Data: 06/06/2019
programa
{
	// Bibliotecas
	inclua biblioteca Util --> ut

	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		inteiro vet[] = {18, 7, 42, 25, 2}
		inteiro aux

		// Apresentação do programa
		escreva("-- Bubble Sort --\n")

		// Imprimir o vetor
		escreva("> Vetor original: ")
		escrevaVetor(vet)

		// Ordenar o vetor via Bubble sort
		para(inteiro j = 0; j < ut.numero_elementos(vet)-1; j++) {
			para(inteiro i = 0; i < ut.numero_elementos(vet)-1-j; i++) {
				se(vet[i] > vet[i+1]) {
					aux = vet[i]
					vet[i] = vet[i+1]
					vet[i+1] = aux
				}
			}
		}

		// Imprimir o vetor ordenado
		escreva("\n> Vetor ordenado: ")
		escrevaVetor(vet)
	}

	// Função: escrevaVetor
	// Descrição: imprime o conteúdo de um vetor passado
	funcao escrevaVetor(inteiro vet[]) {
		para(inteiro i = 0; i < ut.numero_elementos(vet); i++)
			escreva(vet[i], " ")
	}
}



/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 708; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */