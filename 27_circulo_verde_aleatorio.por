// Algoritmo: Círculo verde aleatório
// Descrição: desenha um círculo verde em posições aleatórias a cada execução.
// Autor: Alyppyo Coutinho
// Data: 30/04/2019
programa
{
	// Bibliotecas
	inclua biblioteca Graficos --> grf
	inclua biblioteca Mouse --> mse
	inclua biblioteca Util --> ut

	// Função principal
	funcao inicio()
	{
		// Declaração de constantes
		const inteiro LARGURA_JANELA = 500, ALTURA_JANELA = 600
		const inteiro DIAMETRO = 100

		// Iniciar o modo gráfico
		grf.iniciar_modo_grafico(falso)

		// Definir as dimensões das janelas
		grf.definir_dimensoes_janela(LARGURA_JANELA, ALTURA_JANELA)

		// Definir o título da janela (apresentação do programa)
		grf.definir_titulo_janela("Círculo Verde Aleatório")

		// Desenhar círculo
		grf.definir_cor(grf.COR_VERDE)
		grf.desenhar_elipse(ut.sorteia(0, LARGURA_JANELA - DIAMETRO), // Coordenada x
						ut.sorteia(0, ALTURA_JANELA - DIAMETRO),  // Coordenada y
						DIAMETRO,   // Largura do quadrado que contém o círculo
						DIAMETRO,   // Altura do quadrado que contém o círculo
						verdadeiro) // Preenchimento

		// Renderizar objetos na tela
		grf.renderizar()

		// Segurar a janela
		mse.ler_botao()
		mse.ler_botao()

		// Encerrar o modo gráfico
		grf.encerrar_modo_grafico()
	}
}

/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1199; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */