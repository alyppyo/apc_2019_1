// Algoritmo: Formas de pagamento
// Descrição: recebe do usuário o valor total de uma venda e a opção de forma
//		    de pagamento. Em posse dessas informações, realiza o cálculo do
//		    valor final que será pago.
// Autor: Alyppyo Coutinho
// Data: 26/03/2019
programa
{
	// Função principal
	funcao inicio()
	{
		// Declaração de constantes
		const inteiro BOLETO = 1
		const inteiro CARTAO_A_VISTA = 2
		const inteiro CARTAO_ATE_3X  = 3
		const inteiro CARTAO_MAIS_3X = 4
		
		// Declaração de variáveis
		real valorCompra, valorFinalCompra = 0.0
		inteiro opcao // Forma de pagamento escolhida pelo usuário
		logico opcaoValida = verdadeiro // Flag que indica se o usuário escolheu uma opção válida
		
		// Apresentação do programa
		escreva("-- Forma de Pagamento --\n")
		
		// Solicitar ao usuário o valor da compra
		escreva("- Entre com o valor da compra (R$): ")
		leia(valorCompra)
		
		// Apresentar as formas de pagamento
		escreva("\n- Formas de pagamento disponíveis: ")
		escreva("\n1) Boleto (15% de desconto)")
		escreva("\n2) Cartão à vista (10% de desconto)")
		escreva("\n3) Cartão - até 3x (sem desconto)")
		escreva("\n4) Cartão - Mais de 3x (20% de acréscimo)")
		
		// Solicitar ao usuário a forma de pagamento
		escreva("\n\n- Escolha uma forma de pagamento (1-4): ")
		leia(opcao)
		
		// Calcular o valor final da compra baseado na forma de pagamento escolhida
		escolha (opcao) {
			caso BOLETO:
				valorFinalCompra = valorCompra * 0.85
				pare
			caso CARTAO_A_VISTA:
				valorFinalCompra = valorCompra * 0.9
				pare
			caso CARTAO_ATE_3X:
				valorFinalCompra = valorCompra
				pare
			caso CARTAO_MAIS_3X:
				valorFinalCompra = valorCompra * 1.2
				pare
			caso contrario:
				escreva("\n> Opção inválida! As opções estão no intervalo de 1 a 4.")
				opcaoValida = falso
		}
		
		// Divulgar o valor final da compra
		se(opcaoValida) {
			escreva("\n> Total a pagar: R$ ", valorFinalCompra)
		}
	}
}





/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1808; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */