// Algoritmo: Ponto em área retangular
// Descrição: recebe as coordenadas de dois pontos que delimitam uma
//		    área retangular em um plano cartesiano. Em seguida, recebe
//		    um terceiro ponto e indica se ele está dentro, fora ou na
//		    borda da área.
// Autor: Alyppyo Coutinho
// Data: 21/03/2019
programa
{
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		real x1, y1 // Coordenadas do ponto inferior esquerdo (P1)
		real x2, y2 // Coordenadas do ponto superior direito (P2)
		real x3, y3 // Coordenadas do ponto de teste (P3)
		
		// Apresentação do programa
		escreva("-- Ponto em Área Retangular --\n")
		
		// Solicitar coordenadas dos pontos que delimitam a área retangular
		escreva("- Entre com a coordenada x do ponto inferior esquerdo: ")
		leia(x1)

		escreva("- Entre com a coordenada y do ponto inferior esquerdo: ")
		leia(y1)

		escreva("\n- Entre com a coordenada x do ponto superior direito: ")
		leia(x2)

		escreva("- Entre com a coordenada y do ponto superior direito: ")
		leia(y2)
		
		// Solicitar coordenadas do ponto de teste
		escreva("\n- Entre com a coordenada x do ponto de teste: ")
		leia(x3)

		escreva("- Entre com a coordenada y do ponto de teste: ")
		leia(y3)
		
		// Verificar se o ponto está dentro da área retangular
		se(x3 > x1 e x3 < x2 e y3 > y1 e y3 < y2) {
			escreva("\n> O ponto (", x3, ",", y3, ") está dentro da área retangular!")
		}
		// Verificar se o ponto está fora da área retangular
		senao se(x3 < x1 ou x3 > x2 ou y3 < y1 ou y3 > y2) {
			escreva("\n> O ponto (", x3, ",", y3, ") está fora da área retangular!")			
		}
		// Caso não esteja dentro ou fora, só poderá estar na borda
		senao {
			escreva("\n> O ponto (", x3, ",", y3, ") está na borda da área retangular!")
		}
	}
}





/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1705; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */