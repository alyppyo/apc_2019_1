// Algoritmo: Produtório
// Descrição: recebe diversos valores inteiros não-nulos e os
//		    multiplica em um produtório.
// Autor: Alyppyo Coutinho
// Data: 14/05/2019
programa
{
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		inteiro valor, produtorio = 1

		// Apresentação do programa
		escreva("-- Produtório --\n")

		// Solicitação dos valores para o produtório
		faca {
			escreva("- Entre com um valor: ")
			leia(valor)

			// Adicionar o valor ao produtório
			se(valor != 0) produtorio *= valor
		} enquanto(valor != 0)

		// Divulgar o produtório
		escreva("> Produtório: ", produtorio)
	}
}


/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 629; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */