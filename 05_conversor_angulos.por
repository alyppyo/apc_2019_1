// Algoritmo: Conversor de ângulo
// Descrição: recebe um valor em graus e o converte para radianos.
//		    Calcula também as razões trigonométricas básicas.
// Autor: Alyppyo Coutinho
// Data: 19/03/2019
programa
{
	// Bibliotecas
	inclua biblioteca Matematica --> mat

	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		real graus    // Valor do ângulo recebido pelo usuário
		real radianos // Valor do ângulo convertido para radianos
		
		// Apresentação do programa
		escreva("-- Conversor de Ângulos --\n")
		
		// Solicitar ângulo em graus para o usuário
		escreva("- Entre com o ângulo em graus: ")
		leia(graus)
		
		// Converter o valor do ângulo para radianos
		radianos = graus * mat.PI/180
		
		// Divulgar o resultado da conversão, assim como as razões básicas
		escreva("\n> Ângulo em radianos: ", radianos, "\n")
		escreva("> Seno: ", mat.arredondar(mat.seno(radianos), 4), "\n")
		escreva("> Cosseno: ", mat.arredondar(mat.cosseno(radianos), 4), "\n")
		escreva("> Tangente: ", mat.arredondar(mat.tangente(radianos), 4), "\n")
		escreva("> Cossecante: ", mat.arredondar(1/mat.seno(radianos), 4), "\n")
		escreva("> Secante: ", mat.arredondar(1/mat.cosseno(radianos), 4), "\n")
		escreva("> Cotangente: ", mat.arredondar(1/mat.tangente(radianos), 4), "\n")
	}
}




/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1291; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */