// Algoritmo: Calculadora com 4 operações
// Descrição: recebe dois operandos e um operador do usuário, realiza
//		    o cálculo solicitado e o divulga ao final.
// Autor: Alyppyo Coutinho
// Data: 28/03/2019
programa
{
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		real operando1, operando2 // Operandos fornecidos pelo usuário
		real resultado = 0.0 // Resultado da operação solicitada
		caracter operacao // Uma das 4 operações básicas disponíveis na calculadora
		
		// Apresentação do programa
		escreva("-- Calculadora com 4 Operações --\n")
		
		// Solicitar o primeiro operando
		escreva("- Entre com o primeiro operando: ")
		leia(operando1)
		
		// Solicitar a operação
		escreva("- Entre com uma operação (+, -, *, /): ")
		leia(operacao)
		
		// Solicitar o segundo operando
		escreva("- Entre com o segundo operando: ")
		leia(operando2)
		
		// Verificar qual é a operação a ser realizada
		// e realizar o cálculo de acordo
		escolha(operacao) {
			caso '+': // Adição
				resultado = operando1 + operando2
				pare
			caso '-': // Subtração
				resultado = operando1 - operando2
				pare
			caso '*': // Multiplicação
				resultado = operando1 * operando2
				pare
			caso '/': // Divisão
				resultado = operando1 / operando2
				pare
			caso contrario:
				escreva("\n> Operação inválida!")
		}
		
		// Divulgação do resultado
		escreva("\n> ", operando1, " ", operacao, " ", operando2, " = ", resultado)
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 383; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */