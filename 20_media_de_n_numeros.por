// Algoritmo: Média de N números
// Descrição: recebe um valor N do usuário indicando quantos números
//		    serão adicionados à média calculada, calculando-a poste-
//		    riormente.
// Autor: Alyppyo Coutinho
// Data: 02/04/2019
programa
{	
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		inteiro qntValores
		real	media = 0.0, valor

		// Apresentação do programa
		escreva("-- Média de N Números --\n")

		// Solicitar a quantidade de números que serão lidos
		escreva("- Entre com a quantidade de números para a média: ")
		leia(qntValores)

		// Ler a quantidade de valores estipuladas pelo usuário
		para(inteiro cont = 1; cont <= qntValores; cont++) {
			// Solicitar novo valor
			escreva("- Entre com o ", cont, "º valor: ")
			leia(valor)

			// Acumular os valores em uma soma
			media += valor
		}

		// Calcular a média
		media /= qntValores

		// Divulgar a média dos valores
		escreva("\n> Média: ", media)
	}
}




/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 875; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */