// Algoritmo: Par ou ímpar
// Descrição: recebe um valor do usuário e indica se ele é par ou ímpar.
// Autor: Alyppyo Coutinho
// Data: 21/03/2019
programa
{
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		inteiro valor // Valor a ter sua paridade testada
		
		// Apresentação do programa
		escreva("-- Par ou Ímpar --\n")
		
		// Solicitar valor ao usuário
		escreva("- Entre com um número inteiro: ")
		leia(valor)
		
		// Verificar e divulgar se o valor é par ou ímpar
		se(valor%2 == 0) {
			escreva("> O número ", valor, " é par!")	
		}
		senao {
			escreva("> O número ", valor, " é ímpar!")
		}
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 578; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */