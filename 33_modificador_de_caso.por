// Algoritmo: Modificador de caso
// Descrição: recebe um texto do usuário e o converte para versões em maiúsculo,
//		    minúsculo e alternado.
// Autor: Alyppyo Coutinho
// Data: 16/05/2019
programa
{
	// Biblioteca
	inclua biblioteca Texto --> txt

	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		cadeia texto

		// Apresentação do programa
		escreva("-- Modificador de Caso --\n")

		// Solicitar ao usuário que entre com um texto
		escreva("- Entre com um texto: ")	
		leia(texto)

		// Versão em maiúsculas e em minúsculas
		escreva("\n> Maiúsculas: ", txt.caixa_alta(texto))
		escreva("\n> Minúsculas: ", txt.caixa_baixa(texto))

		// Percorrer texto caractere a caractere
		escreva("\n> Alternando o caso: ")
		para(inteiro i = 0; i < txt.numero_caracteres(texto); i++) {
			// Converter caracteres de índice par em maiúsculas
			se(i%2 == 0) escreva(txt.caixa_alta(txt.obter_caracter(texto, i)+""))
			senao escreva(txt.caixa_baixa(txt.obter_caracter(texto, i)+""))		
		}
	}
}






/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 970; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */