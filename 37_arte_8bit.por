// Algoritmo: Arte 8-bit
// Descrição: imprime o conteúdo de uma matriz em modo gráfico, criando
//  		    quadrados coloridos onde há o valor 1 e pretos onde há o
//		    valor zero.
// Autor: Alyppyo Coutinho
// Data: 21/05/2019
programa
{
	// Bibliotecas
	inclua biblioteca Graficos --> grf
	inclua biblioteca Teclado --> tcl
	inclua biblioteca Util --> ut

	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		inteiro tamQuad = 20
		inteiro matriz[][] = { {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
						   {0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0},
						   {0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0},
						   {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
						   {0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0},
						   {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
						   {0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0},
						   {0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0},
						   {0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0},
						   {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} }

		// Iniciar o modo gráfico
		grf.iniciar_modo_grafico(falso)

		// Configurar a janela
		grf.definir_dimensoes_janela(tamQuad * ut.numero_colunas(matriz),
							    tamQuad * ut.numero_linhas(matriz))
		grf.definir_titulo_janela("Arte 8-bit")							    

		// Definir cor
		grf.definir_cor(grf.COR_VERDE)
		para(inteiro i = 0; i < ut.numero_linhas(matriz); i++) {
			para(inteiro j = 0; j < ut.numero_colunas(matriz); j++) {
				se(matriz[i][j] == 1)
					grf.desenhar_retangulo(j*tamQuad, i*tamQuad, tamQuad, tamQuad, falso, verdadeiro)
			}
		}
		grf.renderizar()

		// Loop principal do modo gráfico
		enquanto(nao tcl.tecla_pressionada(tcl.TECLA_ESC) ) {}

		// Encerrar o modo gráfico
		grf.encerrar_modo_grafico()
	}
}






/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 457; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */