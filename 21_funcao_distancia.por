// Algoritmo: Distância entre dois pontos
// Descrição: dados dois pontos no espaço cartesiano fornecidos pelo usuário,
//		    calcula e indica a distância entre eles.
// Autor: Alyppyo Coutinho
// Data: 16/04/2019
programa
{
	// Bibliotecas
	inclua biblioteca Matematica --> mat

	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		real x1, y1 // Coordenadas do Ponto 1
		real x2, y2 // Coordenadas do Ponto 2

		// Apresentação do programa
		escreva("-- Distância entre os Pontos --\n")

		// Solicitar as coordenadas dos pontos ao usuário
		escreva("- Entre com a coordenada x do ponto 1: ")
		leia(x1)

		escreva("- Entre com a coordenada y do ponto 1: ")
		leia(y1)

		escreva("- Entre com a coordenada x do ponto 2: ")
		leia(x2)

		escreva("- Entre com a coordenada y do ponto 2: ")
		leia(y2)

		// Divulgar a distância
		escreva("\n> Distância entre os pontos: ", distancia(x1, y1, x2, y2))
	}

	// Função: distancia
	// Descrição: recebe as coordenadas de dois pontos no espaço cartesiano e calcula sua distância.
	funcao real distancia(real p1x, real p1y, real p2x, real p2y)
	{
		// Declaração de variáveis
		real dist

		// Calcular a distância
		dist = mat.raiz(mat.potencia(p2x-p1x, 2.0)+mat.potencia(p2y-p1y, 2.0), 2.0)

		// Retornar o valor da distância
		retorne dist
	}
}






/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1115; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */