// Algoritmo: Números de 1 a 20
// Descrição: imprime os números de 1 a 20 na tela.
// Autor: Alyppyo Coutinho
// Data: 28/03/2019
programa
{	
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		inteiro cont = 1
		
		// Apresentação do programa
		escreva("-- Números de 1 a 20 --\n")
		
		// Impressão dos valores
		enquanto(cont <= 20) {
			escreva(cont, " ")

			// Possibilidades de atualização
			// 1) cont = cont + 1
			// 2) cont += 1
			cont++
		}
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 476; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */