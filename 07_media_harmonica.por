// Algoritmo: Média harmônica
// Descrição: recebe 4 valores do usuário e calcula a média harmônica
//		    correspondente.
// Autor: Alyppyo Coutinho
// Data: 19/03/2019
programa
{
	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		real numero1, numero2, numero3, numero4 // Valores fornecidos pelo usuário		real media
		real mediaHarmonica
		
		// Apresentação do programa
		escreva("-- Média Harmônica --\n")
		
		// Solicitar os 4 valores ao usuário
		escreva("- Entre com o primeiro número: ")
		leia(numero1)

		escreva("- Entre com o segundo número: ")
		leia(numero2)

		escreva("- Entre com o terceiro número: ")
		leia(numero3)

		escreva("- Entre com o quarto número: ")
		leia(numero4)
		
		// Calcular a média harmônica
		mediaHarmonica = 4/(1/numero1 + 1/numero2 + 1/numero3 + 1/numero4)
		
		// Divulgar a média harmônica calculada		
		escreva("\n> Média harmônica: ", mediaHarmonica)
	}
}


/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 924; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */