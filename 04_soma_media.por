// Algoritmo: Soma e média aritmética
// Descrição: Calcula a soma e a média aritmética de 3 valores reais
//		    fornecidos pelo usuário.
// Autor: Alyppyo Coutinho
// Data: 19/03/2019
programa
{
	// Bibliotecas
	inclua biblioteca Matematica --> mat

	// Função principal
	funcao inicio()
	{
		// Declaração de variáveis
		real valor1, valor2, valor3 // Valores fornecidos pelo usuário
		real soma  // Soma dos valores fornecidos
		real media // Média aritmética dos valores fornecidos

		// Apresentação do programa
		escreva("-- Soma e Média Aritmética --\n")
		
		// Solicitar valores para o usuário
		escreva("- Entre com o primeiro valor: ")
		leia(valor1)

		escreva("- Entre com o segundo valor: ")
		leia(valor2)

		escreva("- Entre com o terceiro valor: ")
		leia(valor3)
		
		// Somar os valores e tirar a média aritmética
		soma = valor1 + valor2 + valor3
		media = soma/3

		// Limpar a tela
		limpa()

		// Apresentação do programa
		escreva("-- Soma e Média Aritmética --\n")
		
		// Divulgar os resultados arredondados
		escreva("> ", valor1, " + ", valor2, " + ", valor3, " = ", mat.arredondar(soma, 1), "\n")
		escreva("> Média: ", mat.arredondar(media, 1))
	}
}




/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1133; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */