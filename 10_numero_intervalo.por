// Algoritmo: Número em intervalo
// Descrição: recebe dois valores que delimitam um intervalo e um
//		    terceiro que será verificado. O algoritmo informa se
//		    o terceiro valor está dentro, antes ou depois do
//		    intervalo definido.
// Autor: Alyppyo Coutinho
// Data: 21/03/2019
programa
{
	
// Função principal
	funcao inicio()
	{
		// Declarar variáveis
		real inicioIntervalo, fimIntervalo // Valores que delimitam o intervalo
		real valorTestado // Valor a ser testado
		
		// Apresentar o programa
		escreva("-- Número em Intervalo --\n")
		
		// Solicitar os valores que delimitam o intervalo
		escreva("- Entre com o valor de início do intervalo: ")
		leia(inicioIntervalo)

		escreva("- Entre com o valor de fim do intervalo: ")
		leia(fimIntervalo)
		
		// Solicitar o valor a ser testado
		escreva("- Entre com o valor a ser testado: ")
		leia(valorTestado)
		
		// Verificar posicionamento do valor testado no intervalo definido 
		se(valorTestado >= inicioIntervalo e valorTestado <= fimIntervalo) {
			escreva("\n> ", valorTestado, " está dentro do intervalo.")
		}
		senao se(valorTestado < inicioIntervalo) {
			escreva("\n> ", valorTestado, " aparece antes do intervalo.")
		}
		senao {
			escreva("\n> ", valorTestado, " aparece depois do intervalo.")
		}
	}
}

/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1216; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */